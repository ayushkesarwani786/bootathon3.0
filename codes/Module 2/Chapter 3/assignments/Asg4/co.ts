function check()
{
    //Initializing the vextices
    var t11:HTMLInputElement=<HTMLInputElement>document.getElementById("t11");
    var t12:HTMLInputElement=<HTMLInputElement>document.getElementById("t12");
    var t21:HTMLInputElement=<HTMLInputElement>document.getElementById("t21");
    var t22:HTMLInputElement=<HTMLInputElement>document.getElementById("t22");
    var t31:HTMLInputElement=<HTMLInputElement>document.getElementById("t31");
    var t32:HTMLInputElement=<HTMLInputElement>document.getElementById("t32");
    var t41:HTMLInputElement=<HTMLInputElement>document.getElementById("t41");
    var t42:HTMLInputElement=<HTMLInputElement>document.getElementById("t42");

    //Converting vertices into number format
    var x1:number= parseFloat(t11.value);
    var y1:number= parseFloat(t12.value);
    var x2:number= parseFloat(t21.value);
    var y2:number= parseFloat(t22.value);
    var x3:number= parseFloat(t31.value);
    var y3:number= parseFloat(t32.value);
    var x:number= parseFloat(t41.value);
    var y:number= parseFloat(t42.value);

    //Finding area of each triangle
    var a:number=Math.abs(x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2;
    var a1:number=Math.abs(x*(y2-y3)+x2*(y3-y)+x3*(y-y2))/2;
    var a2:number=Math.abs(x1*(y-y3)+x*(y3-y1)+x3*(y1-y))/2;
    var a3:number=Math.abs(x1*(y2-y)+x2*(y-y1)+x*(y1-y2))/2;

    //Finding sum and equating..
    var sum:number=a1+a2+a3;
    var ans:string;
    console.log(a,a1,a2,a3,sum);
    if(Math.abs(a-sum)<0.000001)
    {
        ans="The Point lies Inside the Triangle";
    }
    else
    {
        ans="The Point lies Outside the Triangle"
    }

    document.getElementById("here").innerHTML="<u><b>"+ans+"</b></u>"
    //document.getElementById("here").innerHTML+="<br>values:"+a+" "+a1+" "+a2+" "+a3+" "+sum;


}