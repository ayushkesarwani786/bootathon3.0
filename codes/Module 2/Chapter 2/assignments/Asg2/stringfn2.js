function strfn() {
    var a = document.getElementById("t1");
    var b = a.value;
    var c;
    document.getElementById("display").innerHTML = "<br><br><b>The Original String is the : </b>" + b;
    //TO Upper Case
    c = b.toUpperCase();
    document.getElementById("display").innerHTML += "<br><br><b>String in Upper Case is : </b> " + c;
    //to Lower Case
    c = b.toLowerCase();
    document.getElementById("display").innerHTML += "<br><br><b>String in Lower Case is : </b> " + c;
    //Spliiting the given String
    let ar = b.split(" ");
    document.getElementById("display").innerHTML += "<br><br><b> The splitted string is  : </b> " + ar;
}
//# sourceMappingURL=stringfn2.js.map