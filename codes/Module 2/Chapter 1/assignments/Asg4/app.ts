var t1 : HTMLInputElement =<HTMLInputElement>document.getElementById("t1");
var t2 : HTMLInputElement =<HTMLInputElement>document.getElementById("t2");
var t3 : HTMLInputElement =<HTMLInputElement>document.getElementById("t3");

function add()
{
    var c:number=parseFloat(t1.value)+parseFloat(t2.value);
    t3.value=c.toString();
}
function sub()
{
    var c:number=parseFloat(t1.value)-parseFloat(t2.value);
    t3.value=c.toString();
}
function mul()
{
    var c:number=parseFloat(t1.value)*parseFloat(t2.value);
    t3.value=c.toString();
}
function div()
{
    try {
        var c:number=parseFloat(t1.value)/parseFloat(t2.value);
        t3.value=c.toString();
    } catch (error) {
        t3.value=(error);
    }  
}
function sin()
{
    var c:number=Math.sin(parseFloat(t1.value));
    t3.value=c.toString();
}
function cos()
{
    var c:number=Math.cos(parseFloat(t1.value));
    t3.value=c.toString();
}
function tan()
{
    var c:number=Math.tan(parseFloat(t1.value));
    t3.value=c.toString();
}
function pow()
{
    var c:number=Math.pow(parseFloat(t1.value),parseFloat(t2.value));
    t3.value=c.toString();
}
function sqrt()
{
    var c:number=Math.sqrt(parseFloat(t1.value));
    t3.value=c.toString();
}
