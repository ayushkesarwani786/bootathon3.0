function triangle()
{
    //Step1: Initialise the variable.
    var t11:HTMLInputElement=<HTMLInputElement>document.getElementById("t11");
    var t12:HTMLInputElement=<HTMLInputElement>document.getElementById("t12");
    var t21:HTMLInputElement=<HTMLInputElement>document.getElementById("t21");
    var t22:HTMLInputElement=<HTMLInputElement>document.getElementById("t22");
    var t31:HTMLInputElement=<HTMLInputElement>document.getElementById("t31");
    var t32:HTMLInputElement=<HTMLInputElement>document.getElementById("t32");
    //Storing the values in Float Data type variables.
    var x1:number=parseFloat(t11.value);
    var y1:number=parseFloat(t12.value);
    var x2:number=parseFloat(t21.value);
    var y2:number=parseFloat(t22.value);
    var x3:number=parseFloat(t31.value);
    var y3:number=parseFloat(t32.value);
    //Calculate The sides of Triangle.
    var a:number=Math.sqrt(Math.pow(y2-y1,2)+Math.pow(x2-x1,2));
    var b:number=Math.sqrt(Math.pow(y3-y2,2)+Math.pow(x3-x2,2));
    var c:number=Math.sqrt(Math.pow(y3-y1,2)+Math.pow(x3-x1,2));

    document.getElementById("x").innerHTML="<b>The sides a, b, c respectively are <b>: "+a+"  "+b+" "+c; 
    //Calculating Area
    var s:number=(a+b+c)/2;
    var area:number=Math.sqrt(s*(s-a)*(s-b)*(s-c));

    document.getElementById("x").innerHTML+="<br><br><b>The Area of Triangle is <b>: "+area; 


}